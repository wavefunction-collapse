// wavefunction-collapse.cc (C) 2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only
//
// For alternative transitions based on tile overlaps instead of image:
// #define ADJACENT_TILES
//
// For images of failure:
// #define PPM_CONTRADICTION
//
// For videos:
// #define PPM_COLLAPSE
//
// For slow videos:
// #define PPM_RIPPLE
//
// To exit on first contradiction
// #define CONTRADICTION_QUIT

#include <cassert>
#include <climits>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <ctime>

#include <algorithm>
#include <exception>
#include <set>
#include <vector>

// random shuffle
template <typename T>
std::vector<T> shuffle(std::vector<T> &a)
{
  std::vector<std::pair<int, size_t>> b;
  b.reserve(a.size());
  for (size_t i = 0; i < a.size(); ++i)
  {
    b.push_back(std::pair<int, size_t>(rand(), i));
  }
  std::sort(b.begin(), b.end());
  std::vector<T> c;
  c.reserve(a.size());
  for (size_t i = 0; i < a.size(); ++i)
  {
    c.push_back(a[b[i].second]);
  }
  return c;
}

// find index of element; input must be sorted
template <typename T>
ssize_t index(const std::vector<T> &a, const T &x)
{
  ssize_t lo = 0;
  ssize_t hi = a.size();
  while (lo + 1 != hi)
  {
    assert(lo < hi);
    assert(a[lo] <= x);
    assert(x <= (a[hi - 1]));
    ssize_t md = (lo + hi) / 2;
    const T &y = a[md];
    if (y == x)
    {
      return md;
    }
    if (y < x)
    {
      lo = md;
    }
    else
    {
      hi = md;
    }
  }
  if (a[lo] == x)
  {
    return lo;
  }
  return -1;
}

struct contradiction : public std::exception
{
  int j, i;
  contradiction(int j, int i) : j(j), i(i) { }
};

struct complete : public std::exception
{
  bool dummy;
};

// RGB
struct colour
{
  uint8_t x[3];
};

bool operator==(const colour &a, const colour &b)
{
  return
    a.x[0] == b.x[0] && 
    a.x[1] == b.x[1] && 
    a.x[2] == b.x[2] ;
}

bool operator!=(const colour &a, const colour &b)
{
  return
    a.x[0] != b.x[0] || 
    a.x[1] != b.x[1] || 
    a.x[2] != b.x[2] ;
}

// arbitrary ordering for sets
bool operator<(const colour &a, const colour &b)
{
  for (int i = 0; i < 3; ++i)
  {
    if (a.x[i] < b.x[i])
    {
      return true;
    }
    if (a.x[i] > b.x[i])
    {
      return false;
    }
  }
  return false;
}

bool operator>(const colour &a, const colour &b)
{
  return b < a;
}

template <int D>
struct tile
{
  colour x[D][D];
};

// arbitrary ordering for sets
template <int D>
bool operator<(const tile<D> &a, const tile<D> &b)
{
  for (int j = 0; j < D; ++j)
  {
    for (int i = 0; i < D; ++i)
    {
      if (a.x[j][i] < b.x[j][i])
      {
        return true;
      }
      if (a.x[j][i] > b.x[j][i])
      {
        return false;
      }
    }
  }
  return false;
}

template <int D>
bool operator<=(const tile<D> &a, const tile<D> &b)
{
  for (int j = 0; j < D; ++j)
  {
    for (int i = 0; i < D; ++i)
    {
      if (a.x[j][i] < b.x[j][i])
      {
        return true;
      }
      if (a.x[j][i] > b.x[j][i])
      {
        return false;
      }
    }
  }
  return true;
}

template <int D>
bool operator==(const tile<D> &a, const tile<D> &b)
{
  for (int j = 0; j < D; ++j)
  {
    for (int i = 0; i < D; ++i)
    {
      if (a.x[j][i] != b.x[j][i])
      {
        return false;
      }
    }
  }
  return true;
}

struct image
{
  int h, w;
  colour *x;
  image(int h, int w) : h(h), w(w), x(new colour[h * w]) { }
  ~image() { delete[] x; }
};

// output uncompressed image
void ppm(FILE *out, const image &o)
{
  std::fprintf(out, "P6\n%d %d\n255\n", o.w, o.h);
  std::fwrite(o.x, 3 * o.w * o.h, 1, out);
  std::fflush(out);
}

// unchecked precondition: tile is within image
template <int D>
tile<D> subtile(const struct image &e, int j, int i)
{
  tile<D> t;
  for (int dj = 0; dj < D; ++dj)
  {
    for (int di = 0; di < D; ++di)
    {
      t.x[dj][di] = e.x[(j + dj) * e.w + (i + di)];
    }
  }
  return t;
}

// extract all tiles
template <int D>
std::vector<tile<D>> subtiles(const struct image &e)
{
  std::set<tile<D>> ts;
  for (int j = 0; j + D < e.h; ++j)
  {
    for (int i = 0; i + D < e.w; ++i)
    {
      ts.insert(subtile<D>(e, j, i));
    }
  }
  std::vector<tile<D>> tv(ts.begin(), ts.end());
  std::sort(tv.begin(), tv.end());
  return tv;
}

// set<int> implemented via vector<bool> for compactness
struct intset
{
  std::vector<bool> x;
  intset()
  {
  }
  intset(size_t k) : x(k + 1)
  {
    insert(k);
  }
  intset(size_t lo, size_t hi) : x(hi)
  {
    for (size_t k = lo; k < hi; ++k)
    {
      insert(k);
    }
  }
  size_t count(size_t k) const
  {
    if (k < x.size())
    {
      return x[k];
    }
    else
    {
      return 0;
    }
  }
  void insert(size_t k)
  {
    x.resize(std::max(x.size(), k + 1));
    x[k] = true;
  }
  void erase(size_t k)
  {
    x.resize(std::max(x.size(), k + 1));
    x[k] = false;
  }
  ssize_t nth(size_t n) const
  {
    for (size_t k = 0; k < x.size(); ++k)
    {
      if (x[k])
      {
        if (n-- == 0)
        {
          return k;
        }
      }
    }
    return -1;
  }
  size_t size() const
  {
    size_t n = 0;
    for (size_t k = 0; k < x.size(); ++k)
    {
      if (x[k])
      {
        ++n;
      }
    }
    return n;
  }
  bool empty() const
  {
    for (size_t k = 0; k < x.size(); ++k)
    {
      if (x[k])
      {
        return false;
      }
    }
    return true;
  }
};

intset intersection(const intset &a, const intset &b)
{
  intset c;
  c.x.resize(std::min(a.x.size(), b.x.size()));
  for (size_t i = 0; i < c.x.size(); ++i)
  {
    c.x[i] = a.x[i] & b.x[i];
  }
  return c;
}

intset union_(const intset &a, const intset &b)
{
  intset c;
  size_t m = std::min(a.x.size(), b.x.size());
  c.x.resize(std::max(a.x.size(), b.x.size()));
  for (size_t i = 0; i < m; ++i)
  {
    c.x[i] = a.x[i] | b.x[i];
  }
  // at most one of these loops will be executed
  for (size_t i = m; i < a.x.size(); ++i)
  {
    c.x[i] = a.x[i];
  }
  for (size_t i = m; i < b.x.size(); ++i)
  {
    c.x[i] = b.x[i];
  }
  return c;
}

template <int N>
struct adjacent
{
  intset x[2*N+1][2*N+1];
};

// extract allowed tile transitions from image
template <int D, int N>
std::vector<adjacent<N>> adjacents_in_image(const struct image &e, const std::vector<tile<D>> &tv)
{
  std::vector<adjacent<N>> a;
  a.resize(tv.size());
  for (int j = 0; j + D < e.h; ++j)
  {
    for (int i = 0; i + D < e.w; ++i)
    {
      int t0 = index(tv, subtile<D>(e, j, i));
      assert(t0 != -1);
      for (int dj = -N; dj <= N; ++dj)
      {
        if (0 <= j + dj && j + dj + D <= e.h)
        {
          for (int di = -N; di <= N; ++di)
          {
            if (dj == 0 && di == 0)
            {
              continue;
            }
            if (0 <= i + di && i + di + D < e.w)
            {
              int t1 = index(tv, subtile<D>(e, j + dj, i + di));
              assert(t1 != -1);
              a[t0].x[N - dj][N - di].insert(t1);
            }
          }
        }
      }
    }
  }
  return a;
}

// extract transitions from tile overlaps
template <int D, int N>
std::vector<adjacent<N>> adjacents_in_tiles(const std::vector<tile<D>> &tv)
{
  std::vector<adjacent<N>> a;
  a.resize(tv.size());
  for (size_t t0 = 0; t0 < tv.size(); ++t0)
  {
    for (size_t t1 = 0; t1 < tv.size(); ++t1)
    {
      for (int dj = -N; dj <= N; ++dj)
      {
        for (int di = -N; di <= N; ++di)
        {
          if (dj == 0 && di == 0)
          {
            continue;
          }
          bool ok = true;
          for (int j = 0; ok && j < D; ++j)
          {
            if (0 <= j + dj && j + dj < D)
            {
              for (int i = 0; ok && i < D; ++i)
              {
                if (0 <= i + di && i + di < D)
                {
                  ok &= tv[t0].x[j][i] == tv[t1].x[j + dj][i + di];
                }
              }
            }
          }
          if (ok)
          {
            a[t0].x[N + dj][N + di].insert(t1);
          }
        }
      }
    }
  }
  return a;
}

// an image made of tile possibilities in superposition
struct tiles
{
  int h, w;
  intset *x;
  tiles(int h, int w, const intset &bg) : h(h), w(w), x(new intset[h * w])
  {
    for (int k = 0; k < h * w; ++k) x[k] = bg;
  }
  ~tiles() { delete[] x; }
};

// collapse a single cell
template <int D>
void collapse(image &img, tiles &o, int j, int i, const std::vector<tile<D>> &tv, const std::vector<int> &w)
{
  int k = j * o.w + i;
  int total = 0;
  for (size_t l = 0; l < o.x[k].x.size(); ++l)
  {
    if (o.x[k].x[l])
    {
      total += w[l];
    }
  }
  int p = rand() % total;
  total = 0;
  for (size_t l = 0; l < o.x[k].x.size(); ++l)
  {
    if (o.x[k].x[l])
    {
      total += w[l];
      if (p <= total)
      {
        o.x[k] = intset(l);
        img.x[k] = flatten_pixel(o, tv, j, i);
        break;
      }
    }
  }
#ifdef PPM_COLLAPSE
  ppm(stdout, img);
#endif
}

// propagate constraints recursively
template <int D, int N>
void ripple(image &img, tiles &o, int j, int i, const std::vector<tile<D>> &tv, const std::vector<adjacent<N>> &a)
{
  std::vector<std::pair<int, int>> candidates;
  for (int dj = -N; dj <= N; ++dj)
  {
    if (0 <= j + dj && j + dj < o.h)
    {
      for (int di = -N; di <= N; ++di)
      {
        if (dj == 0 && di == 0)
        {
          continue;
        }
        if (0 <= i + di && i + di < o.w)
        {
          candidates.push_back(std::pair<int, int>(dj, di));
        }
      }
    }
  }
  candidates = shuffle(candidates);
  for (const auto & [ dj, di ] : candidates)
  {
    int k = (j + dj) * o.w + (i + di);
    size_t s0 = o.x[k].size();
    intset u;
    for (size_t t = 0; t < o.x[j * o.w + i].x.size(); ++t)
    {
      if (o.x[j * o.w + i].x[t])
      {
        u = union_(u, a[t].x[N - dj][N - di]);
      }
    }
    o.x[k] = intersection(o.x[k], u);
    img.x[k] = flatten_pixel(o, tv, j + dj, i + di);
#ifdef PPM_RIPPLE
    ppm(stdout, img);
#endif
    size_t s1 = o.x[k].size();
    if (s1 == 0)
    {
#ifdef PPM_CONTRADICTION
      ppm(stdout, img);
#endif
      throw contradiction{ j + dj, i + di };
    }
    else if (s1 < s0)
    {
      ripple<D, N>(img, o, j + dj, i + di, tv, a);
    }
  }
}

// pick a lowest entropy non-collapsed cell at random
// TODO actually use entropy
// currently assumes equal tile probabilities
std::pair<int, int> target(image &img, tiles &o)
{
  
  int mj = -1, mi = -1;
  size_t me = INT_MAX;
  int count = 0;
  for (int j = 0; j < o.h; ++j)
  {
    for (int i = 0; i < o.w; ++i)
    {
      size_t s = o.x[j * o.w + i].size();
      if (s > 1)
      {
        if (s < me)
        {
          mj = j;
          mi = i;
          me = s;
          count = 1;
        }
        else if (s == me)
        {
          if ((rand() % ++count) == 0)
          {
            mj = j;
            mi = i;
            me = s;                        
          }
        }
      }
    }
  }
  if (mj == -1 || mi == -1)
  {
    ppm(stdout, img);
    throw complete{};
  }
  return std::pair<int, int>(mj, mi);
}

// compute the colour of a pixel based on the superpositions of its neighbours
template<int D>
colour flatten_pixel(const tiles &t, const std::vector<tile<D>> &tv, int j, int i)
{
  const colour error = { 0, 0, 255 };
  size_t s0 = t.x[j * t.w + i].size();
  if (s0 == 0)
  {
    return error;
  }
  else if (s0 == 1)
  {
    return tv[t.x[j * t.w + i].nth(0)].x[D/2][D/2];
  }
  else
  {
    int rgb[4] = { 0, 0, 0, 0 };
    for (int dj = -(D/2); dj <= D/2; ++dj)
    {
      if (0 <= j + dj && j + dj < t.h)
      {
        for (int di = -(D/2); di <= D/2; ++di)
        {
          if (0 <= i + di && i + di < t.w)
          {
            int q = (j + dj) * t.w + (i + di);
            size_t s = t.x[q].x.size();
            for (size_t k = 0; k < s; ++k)
            {
              if (t.x[q].x[k])
              {
                colour c = tv[k].x[D/2 - dj][D/2 - di];
                rgb[0] += c.x[0];
                rgb[1] += c.x[1];
                rgb[2] += c.x[2];
                rgb[3] += 1;
              }
            }
          }
        }
      }
    }
    if (rgb[3] == 0)
    {
      rgb[3] = 1; // prevent division by 0
    }
    return colour{ uint8_t(rgb[0] / rgb[3]), uint8_t(rgb[1] / rgb[3]), uint8_t(rgb[2] / rgb[3]) };
  }
}

// flatten a whole image
template<int D>
image flatten(const tiles &t, const std::vector<tile<D>> &tv)
{
  image o(t.h, t.w);
  for (int j = 0; j < o.h; ++j)
  {
    for (int i = 0; i < o.w; ++i)
    {
      o.x[j * o.w + i] = flatten_pixel(t, tv, j, i);
    }
  }
  return o;
}

// compute probability distribution of tiles in an image
template <int D>
std::vector<int> weights(const image &in, const std::vector<tile<D>> &tv)
{
  std::vector<int> w;
  w.resize(tv.size());
  for (int j = 0; j + D < in.h; ++j)
  {
    for (int i = 0; i + D < in.w; ++i)
    {
      ssize_t t = index(tv, subtile<D>(in, j, i));
      assert(0 <= t);
      assert(t < w.size());
      w[t]++;
    }
  }
  return w;
}

// main program
int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  std::srand(std::time(0));

  // parameters to change
  const int D = 3; // tile size, FIXME must be odd
  const int W = 128; // output image width
  const int H = 72; // output image height

  const int N = (D + 1) / 2;
  int w, h;
  char c;
  if (3 == std::scanf("P6 %d %d 255%c", &w, &h, &c) && w > 0 && h > 0 && c == '\n')
  {
    struct image in(h, w);
    if (1 == std::fread(in.x, 3 * w * h, 1, stdin))
    {
      std::vector<tile<D>> tv = subtiles<D>(in);
      std::fprintf(stderr, "%lu tiles\n", tv.size());
#ifdef ADJACENT_TILES
      std::vector<adjacent<N>> a = adjacents_in_tiles<D, N>(tv);
#else
      std::vector<adjacent<N>> a = adjacents_in_image<D, N>(in, tv);
#endif
      size_t s = 0;
      for (size_t i = 0; i < a.size(); ++i)
      {
        for (int dj = -N; dj <= N; ++dj)
        {
          for (int di = -N; di <= N; ++di)
          {
            s += a[i].x[N + dj][N + di].size();
          }
        }
      }
      std::fprintf(stderr, "%lu transitions\n", s);
      std::vector<int> w = weights(in, tv);
      intset bg(0, tv.size());
      int contradictions = 0;
      while (true)
      {
        try
        {
          tiles t(H, W, bg);
          image img = flatten(t, tv);
#if defined(PPM_COLLAPSE) || defined(PPM_RIPPLE)
          ppm(stdout, img);
#endif
          while (true)
          {
            const auto & [ j, i ] = target(img, t);
            collapse(img, t, j, i, tv, w);
            ripple<D, N>(img, t, j, i, tv, a);
          }
        }
        catch (contradiction &e)
        {
          contradictions++;
#ifdef CONTRADICTION_QUIT
          std::fprintf(stderr, "%d contradictions\n", contradictions);
          return 1;
#else
          continue;
#endif
        }
        catch (complete &e)
        {
          std::fprintf(stderr, "%d contradictions\n", contradictions);
          return 0;
        }
      }
    }
  }
  return 1;
}
