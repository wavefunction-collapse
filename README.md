# Wavefunction Collapse

- <https://mathr.co.uk/web/wavefunction-collapse.html>
- <https://code.mathr.co.uk/wavefunction-collapse>

## Usage

```
git clone https://code.mathr.co.uk/wavefunction-collapse.git
cd wavefunction-collapse
make
pngtopnm < in.png |
./wavefunction-collapse |
pnmtopng > out.png
```

## Options

See start of `wavefunction-collapse.cc` for tuneable options.
See start of `main()` function at the bottom for output size options.
TODO: make these configurable at runtime instead of compile time.

## Legal

wavefunction-collapse.cc -- image synthesis from examples

Copyright (C) 2023 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.


--- 
<https://mathr.co.uk>
